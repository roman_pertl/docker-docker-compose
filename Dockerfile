FROM docker:stable

RUN apk --no-cache upgrade && apk add --no-cache bash curl git py-pip py3-pip python-dev libffi-dev openssl-dev gcc libc-dev make && pip install -U --no-cache-dir docker-compose==1.23.0
              
